FROM registry-svc.default.svc.cluster.local:5000/flask-base

COPY . /app

WORKDIR /app

RUN pip install flask

EXPOSE 5000

ENV FLASK_APP=main

CMD ["flask","run","--host=0.0.0.0"]